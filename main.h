/*
 * Texas Instruments IMG video encoder driver test application header
 *
 * Copyright (c) 2019 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/videodev2.h>

#define MAX_CAPBUFS 2
#define MAX_OUTBUFS 2
#define MAX_PLANES 3

#define ALIGN(x,a) (((x) + (a) - 1L) & ~((a) - 1L))
#define HW_ALIGN 64
struct tienc_buffer {
	void *mapped;
	int offset;
	int length;
	unsigned int bo_handle;
	int dbuf_fd;
};

/*
 * struct tienc_format: Describes formats
 *
 * name: Name of format for finding
 * type: V4L2 type of format (input/output)
 * fourcc: V4L2 fourcc
 * size_num: Numerator for image buffer size calculation
 * size_den: Denominator for image buffer size calculation
 * n_planes: Number of buffer planes
 * width: Image width
 * height: Image height
 * stride: Image stride
 * size: Calculated size of image data
 * bytes_pp: Number of bytes per pixel
 * memory: Type of memory to use
 */
struct tienc_format {
	char *name;
	uint32_t type;
	uint32_t fourcc;
	int size_num;
	int size_den;
	int n_planes;
	int width;
	int height;
	int stride;
	int size;
	int bytes_pp;
	enum v4l2_memory memory;
};
